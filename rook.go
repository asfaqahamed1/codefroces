package main

import "fmt"

func main() {
	var testCase int
	var positions []string
	fmt.Scanf("%d\n", &testCase)
	for i := 0; i < testCase; i++ {
		var value string
		fmt.Scanf("%s\n", &value)
		positions = append(positions, value)
	}

	for _, position := range positions {
		for i, char := range position {
			if i == 0 {
				for j, _ := range []uint8{1, 2, 3, 4, 5, 6, 7, 8} {
					str := fmt.Sprintf("%c%d", char, j+1)
					if str != position {
						fmt.Println(str)
					}
				}
			} else {
				for _, ch := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'} {
					str := fmt.Sprintf("%c%c", ch, char)
					if str != position {
						fmt.Println(str)
					}
				}
			}
		}
	}
}
