package main

import (
	"fmt"
)

func main() {
	var questions int
	result := [3]int{0, 0, 0}
	fmt.Scanf("%d\n", &questions)
	for _ = range questions {
		fmt.Scanf("%d %d %d\n", &result[0], &result[1], &result[2])
		if result[0]&(result[1]|result[2])|(result[1]&result[2]) == 0 {
			questions = questions - 1
		}
	}
	println(questions)
}
