package main

import ("fmt")
func main() {
	var num int
	fmt.Scanf("%d\n", &num)
	if num!=2 && num % 2 == 0{
		fmt.Println("YES")
		return
	}
	fmt.Println("NO")
} 
