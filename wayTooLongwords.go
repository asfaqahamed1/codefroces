package main

import (
	"fmt"
)

func main() {
	var count int
	fmt.Scanf("%d\n", &count)
	words:= make([]string , count)
	for i := range words {
		fmt.Scanf("%s\n", &words[i])
		if len(words[i]) > 10 {
			words[i] = fmt.Sprintf("%c%d%c",words[i][0],len(words[i]) -2, words[i][len(words[i])-1])
		}
	}
	for i := range words {
		fmt.Printf("%s\n", words[i])
	}
}
